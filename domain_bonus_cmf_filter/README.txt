
-- SUMMARY --

This module provides domain filters for the Content Management Filter module.

-- REQUIREMENTS --

  Content Management Filter module - http://drupal.org/project/cmf

-- CREDITS --

  Gabor Seljan 'sgabe' <http://drupal.org/user/232117>
