
-- SUMMARY --

This module gives ability to enter separate Gmap key for each domain.


-- INSTALLATION --

Go to domain configuration admin/build/domain/conf/%id and see
Google map configuration block.