
-- SUMMARY --

Provides each node with a tab where users can assign that node to a queue.

This module provides a tab for each active domain so users can see the queues as being separated by domain, even though each queue really can contain nodes from multiple domains.

The use case for this is for multiple domains sharing a similar theme with views filtered by nodequeue AND domain. It simplifies the management process of moving nodes across various domains and queues.

The module also makes a few improvements to the standard nodequeue tab.
- You can configure the column labels to make sense for your site admins.
- The node titles are listed (no. of titles is configurable) in the tabs so it's easier to see what you're moving, and what is already in all the queues.
- The titles link not to the nodes' view tabs, but to the nodequeue tab for the same domain, so you save a few clicks and refreshes as you rearrange your queues. 
